import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Product {
    private int id;
    private int price;
    private int length;
    private int width;
    private int height;
    private int weight;

    Product(String line) {
        List<String> list = Arrays.asList(line.split(","));
        Iterator<String> iterator = list.listIterator();

        id = Integer.parseInt(iterator.next());
        price = Integer.parseInt(iterator.next());
        length = Integer.parseInt(iterator.next());
        width = Integer.parseInt(iterator.next());
        height = Integer.parseInt(iterator.next());
        weight = Integer.parseInt(iterator.next());
    }

    public int getVolume() {
        return (length * width * height);
    }

    public double getPriceToVolumeRatio() {
        return (double) price / (double) getVolume();
    }

//    boolean canFit() {
//        if (length <= CustomerPrize.MAX_LENGTH && width <= CustomerPrize.MAX_WIDTH && height <= CustomerPrize.MAX_HEIGHT) {
//            return true;
//        }
//        System.out.println("Cant fit");
//        return false;
//    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", price=" + price +
                ", volume=" + getVolume() +
                ", weight=" + weight +
                ", PriceToVolume=" + getPriceToVolumeRatio() +
                '}';
    }
}