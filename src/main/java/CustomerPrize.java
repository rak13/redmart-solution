import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CustomerPrize {
    public static final int MAX_LENGTH = 45;
    public static final int MAX_WIDTH = 30;
    public static final int MAX_HEIGHT = 35;

    public static final int MAX_VOLUME = (MAX_LENGTH * MAX_WIDTH * MAX_HEIGHT);

    ArrayList<Product> productList = new ArrayList();

    public CustomerPrize(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        int count = 0;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            Product product = new Product(line);
            if (canFit(product)) {
                productList.add(product);
//                System.out.println("count = " + count++ + ", " + product);
            }
        }
        productList.sort((o1, o2) -> {
            if (o1.getVolume() == o2.getVolume()) {
                return o2.getPrice() - o1.getPrice();
            } else {
                return o1.getVolume() - o2.getVolume();
            }
        });
    }

    public static boolean canFit(Product product) {
        if (product.getLength() <= CustomerPrize.MAX_LENGTH && product.getWidth() <= CustomerPrize.MAX_WIDTH
                && product.getHeight() <= CustomerPrize.MAX_HEIGHT) {
            return true;
        }
//        System.out.println("Cant fit");
        return false;
    }

    public void solveWithDLS() throws Exception {
        SolutionDLS solution = new SolutionDLS(productList);
        solution.solve();

    }

    public void solveWithGA() throws Exception {
        SolutionGA solutionGA = new SolutionGA(productList);
        solutionGA.solve();
    }

    public static void main(String[] args) throws Exception {
        CustomerPrize customerPrize = new CustomerPrize("products.csv");
        customerPrize.solveWithGA();
//        customerPrize.solveWithDLS();
    }
}
