import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SolutionGA extends GeneticAlgorithm {
    List<Product> productList;
    //The previous runs converged to this
    static final int productsToCount = 50;

    public SolutionGA(List<Product> products) throws Exception {
        super(productsToCount, 15, 0.6, 0.4);
        products.sort((o1, o2) -> Double.compare(o2.getPriceToVolumeRatio(), o1.getPriceToVolumeRatio()));
        //Products are sorted based on their price to volume ratio

        productList = products.subList(0, productsToCount);
        BufferedWriter writer = new BufferedWriter(new FileWriter("./output.txt", true));

        //from previous runs
        //We can see that the algorithm has converged towards the first few products with most price to volume ratio

        String initialPrediction = "11111111111111101110011100010000000000000000000000";
        Chromosome chromosome = new Chromosome();
        for (int i = 0; i < initialPrediction.length(); i++) {
            if (initialPrediction.charAt(i) == '1') {
                chromosome.setGene(i, true);
            } else {
                chromosome.setGene(i, false);
            }
        }
        List<Chromosome> list = new ArrayList<>();
        list.add(chromosome);
        initialize(list);
    }

    public void solve() {
        for (int i = 0; i < 1000000; i++) {
            generatePopulation();
            if (i % 500 == 0) {
                System.out.println("i = " + i);
                print(getBestChromosome(), false);
                print(getCurrentWorstChromosome(), false);
                print(getCurrentBestChromosome(), false);
                System.out.println("------------------------------------");
            }
        }
    }

    @Override
    public void print(Chromosome chromosome, boolean writeToFile) {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer binaryForm = new StringBuffer();
        List<Boolean> genes = chromosome.getGenes();
        long totalVolume = 0;
        long totalPrice = 0;
        long sumProductIds = 0;
        long totalWeight = 0;
        for (int i = 0; i < genes.size(); i++) {
            if (genes.get(i)) {
                binaryForm.append("1");
                Product product = productList.get(i);
                totalVolume += product.getVolume();
                totalPrice += product.getPrice();
                sumProductIds += product.getId();
                totalWeight += product.getWeight();
                stringBuffer.append("| " + product + " | ");
            } else {
                binaryForm.append("0");
            }
        }

        stringBuffer.append("\n***totalVolume = " + totalVolume + ", MaxVolume = " + CustomerPrize.MAX_VOLUME
                + ",  totalPrice = " + totalPrice + ", sumProductIds = " + sumProductIds + ", totalWeight = " + totalWeight + "\n");
        if (writeToFile) {
            try {
//                String json = mapper.writeValueAsString(chromosome);
                BufferedWriter writer = new BufferedWriter(new FileWriter("./output.txt", true));
                writer.write("\n**********************************\n");
                writer.write(stringBuffer.toString());
                writer.write(binaryForm.toString() + "\n");
                writer.write("**********************************\n\n");
                writer.close();
            } catch (Exception e) {
                System.out.println("Cannot write");
            }
        }
        System.out.println(binaryForm);
        System.out.println(stringBuffer);
    }

    @Override
    public boolean isValid(Chromosome chromosome) {
        return ((Integer) chromosome.getProperties().get("totalVolume") < CustomerPrize.MAX_VOLUME);
    }

    @Override
    public void updateProperties(Chromosome chromosome) {
        List<Boolean> genes = chromosome.getGenes();
        int totalPrice = 0;
        int totalWeight = 0;
        int totalVolume = 0;
        for (int i = 0; i < genes.size(); i++) {
            if (genes.get(i)) {
                Product product = productList.get(i);
                totalVolume += product.getVolume();
                totalPrice += product.getPrice();
                totalWeight += product.getWeight();
            }
        }
        Map<String, Object> properties = chromosome.getProperties();
        properties.put("totalPrice", totalPrice);
        properties.put("totalWeight", totalWeight);
        properties.put("totalVolume", totalVolume);
    }

    @Override
    public int compare(Chromosome chromosome1, Chromosome chromosome2) {

        Map<String, Object> properties1 = chromosome1.getProperties();

        Map<String, Object> properties2 = chromosome2.getProperties();

        Integer totalVolume1 = (Integer) properties1.get("totalVolume");
        Integer totalVolume2 = (Integer) properties2.get("totalVolume");

        if (totalVolume1 > CustomerPrize.MAX_VOLUME && totalVolume2 > CustomerPrize.MAX_VOLUME) {
            return totalVolume2 - totalVolume1;
        } else if (totalVolume1 > CustomerPrize.MAX_VOLUME) {
            return -1;
        } else if (totalVolume2 > CustomerPrize.MAX_VOLUME) {
            return 1;
        }

        Integer totalPrice1 = (Integer) properties1.get("totalPrice");
        Integer totalPrice2 = (Integer) properties2.get("totalPrice");


        Integer totalWeight1 = (Integer) properties1.get("totalWeight");
        Integer totalWeight2 = (Integer) properties2.get("totalWeight");
        if (totalPrice1 == totalPrice2) {
            return totalWeight2 - totalWeight1;
        } else {
            return totalPrice1 - totalPrice2;
        }
    }
}
