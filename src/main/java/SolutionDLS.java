import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import static java.lang.System.out;

public class SolutionDLS {
    List<Product> productList;
    Result[][] dp;
    int dir[][];
    int counts = 0;

    SolutionDLS(List<Product> products) {
        products.sort((o1, o2) -> Double.compare(o2.getPriceToVolumeRatio(), o1.getPriceToVolumeRatio()));
        productList = products.subList(0, 50);
        dp = new Result[productList.size() + 1][CustomerPrize.MAX_VOLUME + 1];
        dir = new int[productList.size() + 1][CustomerPrize.MAX_VOLUME + 1];
    }

    public int solve() {
        int sz = productList.size() - 1;
        Result solution = solve(sz, CustomerPrize.MAX_VOLUME, 0);
        out.println("Price = " + solution.price + ", Weight =  " + solution.weight);
        printDir(sz, CustomerPrize.MAX_VOLUME);
        return solution.price;
    }

    public Result solve(int pos, int volume, int depth) {

        if (volume <= 0 || pos < 0) {
            return new Result(0, 0);
        }
        if (dp[pos][volume] == null) {
            Result res = new Result(0, 0);
            for (int i = pos; i >= 0; i--) {
                Product product = productList.get(i);
                if (volume - product.getVolume() >= 0) {
                    Result tmp = solve(i - 1, volume - product.getVolume(), depth + 1);
                    Result cur = new Result(tmp.price, tmp.weight);
                    cur.price += product.getPrice();
                    cur.weight += product.getWeight();
                    if (cur.compare(res) > 0) {
                        res = cur;
                        dir[pos][volume] = i;
                    }
                } else {
                    break;
                }
            }
            dp[pos][volume] = res;
        }
        return dp[pos][volume];
    }

    public void printDir(int pos, int volume) {
        if (volume <= 0 || pos < 0) {
            return;
        }

        Integer index = dir[pos][volume];
        Product product = productList.get(index);
        printDir(index - 1, volume - product.getVolume());
        System.out.println(product);
    }

    class Result {
        public int price = 0, weight = 0;

        @Override
        public String toString() {
            return "Result{" +
                    "price=" + price +
                    ", weight=" + weight +
                    '}';
        }

        Result(){
            price = 0;
            weight = 0;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        Result(int p, int w){
            this.price = p;
            this.weight = w;
        }

        int compare(Result other) {
            if(price == other.price){
                return other.weight - weight;
            }
            else {
                return price - other.price;
            }
        }
    }
}
