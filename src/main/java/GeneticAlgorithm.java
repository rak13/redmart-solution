import java.util.*;

public abstract class GeneticAlgorithm {
    //double value between 0 to 1
    private double crossoverProbability;
    private double mutationProbability;

    private final int chromosomeLength;
    private int populationSize;

    private final Random random = new Random();

    List<Chromosome> population;

    Chromosome bestChromosome;

    int minFitIndexInPopulation;
    int maxFitIndexInPopulation;

    public Chromosome getBestChromosome() {
        return bestChromosome;
    }

    public Chromosome getCurrentBestChromosome() {
        return population.get(maxFitIndexInPopulation);
    }

    public Chromosome getCurrentWorstChromosome() {
        return population.get(minFitIndexInPopulation);
    }

    public GeneticAlgorithm(int chromosomeLength, int populationSize, double crossoverProbability, double mutationProbability) {
        this.chromosomeLength = chromosomeLength;
        this.populationSize = populationSize;
        this.crossoverProbability = crossoverProbability;
        this.mutationProbability = mutationProbability;
        population = new ArrayList<>(populationSize);
        minFitIndexInPopulation = 0;
        maxFitIndexInPopulation = populationSize - 1;
    }

    //To use this GA, you must extend this class and implement the following methods
    public abstract int compare(Chromosome chromosome1, Chromosome chromosome2);
    public abstract void updateProperties(Chromosome chromosome);
    public abstract boolean isValid(Chromosome chromosome);
    public abstract void print(Chromosome chromosome, boolean writeToFile);

    final Comparator<Chromosome> chromosomeComparator = (Chromosome o1, Chromosome o2) -> compare(o1, o2);

    public void initialize() {
        for (int i = 0; i < populationSize; i++) {
            Chromosome chromosome = new Chromosome();
            population.add(chromosome);
        }
        updateMostFit();
    }

    public void initialize(List<Chromosome> list) {
        for (int i = 0; i < populationSize && i < list.size(); i++) {
            population.add(list.get(i));
        }
        for(int i = population.size(); i<populationSize; i++) {
            population.add(new Chromosome(true));
        }
        updateMostFit();
    }

    private void updateMostFit() {
        population.sort(chromosomeComparator);
        Chromosome curBest = getCurrentBestChromosome();
        if (bestChromosome == null || compare(curBest, bestChromosome) > 1 && isValid(curBest)) {
            bestChromosome = new Chromosome(curBest);
            print(bestChromosome, true);
        }
    }

    private Chromosome createPartialChromosome(Chromosome chromosome, int partitionPoint, boolean firstPart){
        Chromosome newChromosome = new Chromosome();
        for(int i = 0; i<chromosomeLength; i++) {
            boolean gene = false;
            if(firstPart && i<=partitionPoint) {
                gene =  chromosome.getGene(i);
            }

            if(!firstPart && i>partitionPoint) {
                gene =  chromosome.getGene(i);
            }
            newChromosome.setGene(i, gene);
        }
        return newChromosome;
    }

    private Chromosome merge(Chromosome chromosome1, Chromosome chromosome2){
        Chromosome newChrom = new Chromosome();
        for(int i = 0; i<chromosomeLength; i++) {
            newChrom.setGene(i, chromosome1.getGene(i) || chromosome2.getGene(i));
        }
        if(random.nextDouble() <= mutationProbability) {
            newChrom.mutate();
        }
        else {
            newChrom.updateAll();
        }
        return newChrom;
    }

    private void crossOver(Chromosome chromosome1, Chromosome chromosome2) {
        if (random.nextDouble() <= crossoverProbability) {

            int partitionPoint = random.nextInt(chromosomeLength);

            /**
             Let part before partition point = A
             Let part after  partition point = B

             from chromosome1 = A1 A2
             from chromosome2 = B1 B2

             A1B2
             A2B1
             A2A1

             B1A2
             B2A1
             B2B1
             */
            Chromosome A1 = createPartialChromosome(chromosome1, partitionPoint, true);
            Chromosome A2 = createPartialChromosome(chromosome1, partitionPoint, false);
            Chromosome B1 = createPartialChromosome(chromosome2, partitionPoint, true);
            Chromosome B2 = createPartialChromosome(chromosome2, partitionPoint, false);

            List <Chromosome> newChromosomes  = new ArrayList<>();
            newChromosomes.add(merge(A1, B2));
            newChromosomes.add(merge(A2, B1));
            newChromosomes.add(merge(A2, A1));

            newChromosomes.add(merge(B1, A2));
            newChromosomes.add(merge(B2, A1));
            newChromosomes.add(merge(B2, B1));

            newChromosomes.sort(chromosomeComparator);

            for(int i = 0; i<newChromosomes.size(); i++) {
                Chromosome cur = newChromosomes.get(i);
                if(isValid(cur) && compare(cur, getCurrentWorstChromosome()) > 0) {
                    population.set(minFitIndexInPopulation, cur);
                    updateMostFit();
                }
                else if(random.nextDouble() <= mutationProbability * mutationProbability * mutationProbability) {
                    //very lucky chromosome may still get into the population
                    population.set(random.nextInt(populationSize), cur);
                    updateMostFit();
                }
            }
        }
    }

    private void mutation(Integer position) {
        if (random.nextDouble() <= mutationProbability) {
            if (position == null) {
                position = random.nextInt(populationSize);
            }
            Chromosome chromosome = population.get(position);
            Chromosome mutated = chromosome.mutate();
            if(isValid(mutated)){
                population.set(position, mutated);
                updateMostFit();
            }
            else if(random.nextDouble() <= mutationProbability * mutationProbability) {
                //lucky chromosome may still get into the population
                population.set(position, mutated);
                updateMostFit();
            }
        }
    }

    public void generatePopulation() {
        for (int i = 0; i < populationSize; i++) {
            for (int j = i + 1; j < populationSize; j++) {
                Chromosome chromosome1 = population.get(i);
                Chromosome chromosome2 = population.get(j);
                crossOver(chromosome1, chromosome2);
            }
            mutation(i);
        }
    }

    class Chromosome {
        /*
            A false in a gene means that gene is not present in the chromosome
            and vice versa
         */
        private List<Boolean> genes;

        private Map <String, Object> properties = new HashMap<>();

        Chromosome(Chromosome chrom) {
            genes = new ArrayList<>(chromosomeLength);
            for (int i = 0; i < chromosomeLength; i++) {
                genes.add(chrom.getGene(i));
            }
            updateAll();
        }

        Chromosome(boolean isRandom) {
            genes = new ArrayList<>(chromosomeLength);
            for (int i = 0; i < chromosomeLength; i++) {
                genes.add(random.nextBoolean());
            }
            updateAll();
        }

        Chromosome() {
            genes = new ArrayList<>(chromosomeLength);
            for (int i = 0; i < chromosomeLength; i++) {
                genes.add(false);
            }
            updateAll();
        }

        public void setGene(int index, boolean value) {
            genes.set(index, value);
        }

        public Boolean getGene(int index) {
            return genes.get(index);
        }

        public Chromosome mutate() {
            Chromosome newChromosome = new Chromosome(this);
            if(random.nextInt()%2 == 0) {
                int numberOfMutations = (int) ((double) random.nextInt(chromosomeLength - 1) * mutationProbability * mutationProbability + 1);
                for (int i = 0; i < numberOfMutations; i++) {
                    int mutationPosition = random.nextInt(chromosomeLength);
                    newChromosome.setGene(mutationPosition, !newChromosome.genes.get(mutationPosition));
                }

            }
            else {
                int pos1 = random.nextInt(chromosomeLength);
                int pos2 = random.nextInt(chromosomeLength);
                boolean gene1 = newChromosome.getGene(pos1);
                boolean gene2 = newChromosome.getGene(pos2);
                newChromosome.setGene(pos1, gene2);
                newChromosome.setGene(pos2, gene1);
            }
            newChromosome.updateAll();
            return newChromosome;
        }

        public Map<String, Object> getProperties(){
            return properties;
        }

        public List<Boolean> getGenes() {
            return genes;
        }

        private void updateAll() {
            updateProperties(this);
        }
    }
}
